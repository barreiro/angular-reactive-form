import { Component, OnChanges, Input } from '@angular/core';
import { FormArray, FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { states, Address, Hero } from '../data-model';
import { HeroService } from '../hero.service';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css']
})
export class HeroDetailComponent implements OnChanges {

  heroForm: FormGroup;
  states = states;
  @Input() hero: Hero;

  constructor(
    private formBuilder: FormBuilder, 
    private heroService: HeroService
    ) {
  	this.createForm();
  }

  ngOnChanges() {
    this.heroForm.reset({
      name: this.hero.name
    });

    this.setAddresses(this.hero.addresses);
  }

  onSubmit() {
    this.hero = this.prepareSaveHero();
    this.heroService.updateHero(this.hero).subscribe();
    this.ngOnChanges();
  }

  prepareSaveHero(): Hero {
    const formModel = this.heroForm.value;

    const secretLaitsDeepCopy: Address[] = formModel.secretLairs.map(
      (address: Address) => Object.assign({}, address)
      );

    const saveHero: Hero = {
      id: this.hero.id,
      name: formModel.name as string,
      addresses: secretLaitsDeepCopy
    }

    return saveHero;
  }

  createForm() {
  	this.heroForm = this.formBuilder.group({
  		name: ['', Validators.required],
      secretLairs: this.formBuilder.array([]),
  		power: '',
  		sidekick: '',
  	});
  }

  setAddresses(addresses: Address[]) {
    const addressFGs = addresses.map(
      address => this.formBuilder.group(address)
      );
    const addressFormArray = this.formBuilder.array(addressFGs);

    this.heroForm.setControl('secretLairs', addressFormArray);
  }

  get secretLairs(): FormArray {
    return this.heroForm.get('secretLairs') as FormArray;
  }

  addLair() {
    this.secretLairs.push(this.formBuilder.group(new Address()));
  }

  revert() {
    this.ngOnChanges();
  }

}
